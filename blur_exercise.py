import utils
import numpy as np
import matplotlib.pyplot as plt

INPUT_IMAGE = "data/imgs/to_blur.jpg"
BLUR_KERNEL = np.matrix('1 2 1; 2 4 2; 1 2 1')
EDGE_KERNEL = np.matrix('-1 -2 -1; 0 0 0; 1 2 1')
MY_KERNEL = np.matrix('-1 -1 -1; 0 0 0; 4 4 4')


def apply_filter(photo, kernel):
    print("Applying conv for kernel %s ..." % kernel)
    dim_x = photo.shape[0] - 1
    dim_y = photo.shape[1] - 1
    kernel_size = kernel.shape[0]

    new_photo = np.zeros(photo.shape, dtype='float32')
    # TODO 1: Apply image convolution operation. Each new pixel should be a
    # weighted average of nearby pixels, with weights given by the kernel.
    # PS: no need to optimize this
    for i in range(1, dim_x):
        for j in range(1, dim_y):
            for k in range(0, kernel_size):
                new_photo[i, j, k] = np.multiply(
                    photo[i - 1:i + 2, j - 1:j + 2, k], kernel).sum()
    new_photo /= new_photo.max()

    return new_photo


def main():
    utils.init_plotting()
    to_plot_images = []

    # load original photo
    photo = plt.imread(INPUT_IMAGE)
    to_plot_images.append((photo, "Original Photo"))

    # apply kernel convolution
    # new_photo = apply_filter(photo, BLUR_KERNEL)
    # to_plot_images.append((new_photo, "Blur kernel"))

    # TODO: 2. try some other kernels
    new_photo = apply_filter(photo, EDGE_KERNEL)
    to_plot_images.append((new_photo, "EDGE_KERNEL"))

    # show images
    utils.plot_images(to_plot_images, is_processed=False)
    plt.show(block=True)


if __name__ == "__main__":
    # main()
    # kernel = BLUR_KERNEL
    # print kernel.shape
    # print kernel[1, 1]
    for i in xrange(3):
        print i

    # TODO: 3. De ce patratice/cuburi?
