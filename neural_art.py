# coding: utf-8

'''
An implementation in theano of Neural Style:

"A Neural Algorithm of Artistic Style",
Leon A. Gatys, Alexander S. Ecker, Matthias Bethge
http://arxiv.org/abs/1508.06576
'''


import utils
import scipy
import theano
import lasagne
import numpy as np
import theano.tensor as T
from lasagne.utils import floatX

import matplotlib.pyplot as plt

IMAGE_SIZE = utils.IMAGE_SIZE
IMGS_PATH = "data/imgs/"
PHOTO_PATH = IMGS_PATH + "photo1.jpg"
ART_PATH = IMGS_PATH + "art1.jpg"


STYLE_LAYERS = ["conv1_1", "conv2_1", "conv3_1", "conv4_1", "conv5_1"]
CONTENT_LAYER = "conv4_2"
LOSS_LAYERS = STYLE_LAYERS + [CONTENT_LAYER]

NO_ITERS = 10
MAX_FUN = 20

new_image = None
f_loss = None
f_grad = None


####################
# LOSS FUNCTIONS
####################
def content_loss(P, X, layer):
    '''
    Content loss is the Euclidean distance between activations in a specific
    layer. It is applied on the (photo, noise) pair.
    '''
    p = P[layer]
    x = X[layer]
    loss = 1. / 2 * ((x - p)**2).sum()
    return loss


def style_loss(A, X, layer):
    '''
    Style loss is the difference between Gram matrices of activations in a
    specific layer. It is applied on the (art, noise) pair.
    '''
    a = A[layer]
    x = X[layer]

    A = utils.gram_matrix(a)
    G = utils.gram_matrix(x)

    N = a.shape[1]
    M = a.shape[2] * a.shape[3]

    loss = 1. / (4 * N**2 * M**2) * ((G - A)**2).sum()
    return loss


def variation_loss(x):
    '''
    A measure for how sudden the color changes between 2 close pixels. We want
    the pass to be smooth, so this loss (= the difference between close pixels)
    to be low.
    '''
    return (((x[:, :, :-1, :-1] - x[:, :, 1:, :-1])**2 +
             (x[:, :, :-1, :-1] - x[:, :, :-1, 1:])**2)**1.25).sum()


def build_loss(art_features, photo_features, gen_features, new_image):
    '''
    Computes total loss, based on activations (*_features). Final loss is a
    weighted loss of content, style and variation.
    '''
    losses = []

    # content loss
    losses.append(1e-3 * content_loss(photo_features,
                                      gen_features, CONTENT_LAYER))

    # style loss
    style_weights = 5e2 * np.ones(5)
    for i, style_layer in enumerate(STYLE_LAYERS):
        weight = style_weights[i]
        losses.append(weight * style_loss(art_features,
                                          gen_features, style_layer))

    # variation loss
    losses.append(1e-8 * variation_loss(new_image))

    total_loss = sum(losses)
    return total_loss


def get_subset_loss_layers(net):
    '''
    The subset of layers considered when computing loss (content and style).
    '''
    layers = {k: net[k] for k in LOSS_LAYERS}
    return layers
####################
# END LOSS FUNCTIONS
####################


####################
# OPTIMIZE
####################
def compute_features(input_photo, net):
    '''
    For a given image, returns all activations from loss layers.

    Activations in one layer (output of the current layer), are the
    features (input) for the next one.
    '''
    layers = get_subset_loss_layers(net)
    outputs = lasagne.layers.get_output(layers.values(), input_photo)

    layers_keys = layers.keys()
    features = {k: v for k, v in zip(layers_keys, outputs)}
    return features


def get_optim_functions(net, art, photo, new_image):
    '''
    Returns symbolic loss function and loss gradient.

    1. Compute activations (only in loss layers) for all images (art, photo
    and new_image).
    2. Using those activations, compute the loss.
    3. Compute gradient in of the loss, with respect to new_image.


    Our purpose is to modify new_image, in order to minimize the loss. So we
    need to know how new_image affects the loss (which pixels should increase
    or decrease). That's why we compute the gradient.
    '''
    # features in loss layers
    art_features = compute_features(art, net)
    photo_features = compute_features(photo, net)
    gen_features = compute_features(new_image, net)

    total_loss = build_loss(art_features, photo_features,
                            gen_features, new_image)
    grad = T.grad(total_loss, new_image)

    # Theano functions to evaluate loss and gradient
    f_loss = theano.function([], total_loss)
    f_grad = theano.function([], grad)
    return f_loss, f_grad


# Helper functions to interface with scipy.optimize
def eval_loss(x0):
    '''
    It calls f_loss(), the function that computes the loss. Before that, it
    changes new_image variable (used in f_loss) with the new input.
    '''
    x0 = floatX(x0.reshape((1, 3, IMAGE_SIZE, IMAGE_SIZE)))
    new_image.set_value(x0)
    return f_loss().astype("float64")


def eval_grad(x0):
    '''
    It calls f_grad(), the function that computes the loss. Before that, it
    changes new_image variable (used in f_loss) with the new input.
    '''
    x0 = floatX(x0.reshape((1, 3, IMAGE_SIZE, IMAGE_SIZE)))
    new_image.set_value(x0)
    return np.array(f_grad()).flatten().astype("float64")


def optimize(eval_loss, eval_grad, x0, max_funct_iters):
    '''
    After evaluating the loss and its gradient, "fmin_l_bfgs_b" internally
    updates the new_image, using its specific update rule.

    http://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.optimize.fmin_l_bfgs_b.html
    '''
    scipy.optimize.fmin_l_bfgs_b(
        eval_loss, x0.flatten(), fprime=eval_grad, maxfun=max_funct_iters)
####################
# END OPTIMIZE
####################


def show_progress(step, to_plot, x0):
    plt.close("all")
    print("Step %i..." % step)
    if (step < NO_ITERS - 1):
        to_plot.append((x0, "Step %i" % step))
    else:
        to_plot.append((x0, "DONE"))
    utils.plot_images(to_plot)


def main():
    global f_loss, f_grad
    global new_image

    # plot asynchronous
    utils.init_plotting()

    # Stage 1. Prepare input:
    # load art, photo and the new_image, initialized with random noise
    art, photo = utils.get_photos(ART_PATH, PHOTO_PATH)
    new_image = utils.generate_noise(IMAGE_SIZE)

    # Stage 2. Initialize the networ compute_featuresk architecture:
    # Use a "good for classifying" network architecture: VGG
    net = utils.get_vgg_model()

    # Stage 3. Symbolic values for function and gradient
    print("Compute loss and grad formula...")
    f_loss, f_grad = get_optim_functions(net, art, photo, new_image)

    # plotting images
    to_plot = []
    to_plot.append((art, "Art"))
    to_plot.append((photo, "Photo"))

    # Perform small steps in optimizing the loss by modifying the new_image
    # (the generated image), and observe the progress
    x0 = utils.tensor_to_4D(new_image, IMAGE_SIZE)
    for step in range(NO_ITERS):
        show_progress(step, to_plot, x0)

        # optimize (one step) = loss minimization
        optimize(eval_loss, eval_grad, x0, MAX_FUN)
        x0 = utils.tensor_to_4D(new_image, IMAGE_SIZE)

    plt.show(block=True)


if __name__ == "__main__":
    main()
