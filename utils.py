# coding: utf-8

import os
import theano
import pickle
import platform
import numpy as np
import skimage.transform
import theano.tensor as T
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

import lasagne
from lasagne.utils import floatX

from lasagne.layers import InputLayer
from lasagne.layers import Conv2DLayer as Convolution
from lasagne.layers import Pool2DLayer as Pooling


SHOW_TIME = 0.000001

# precomputed for ImageNet dataset
MEAN_VALUES = np.array([104, 117, 123]).reshape((3, 1, 1))

# generated image size (square img)
IMAGE_SIZE = 90

# 600 -> 0.001, 0.2e6
OUT_FOLDER_NAME = "output"
WEIGHTS_PKL_FILE_WIN = "data/model/vgg19_normalized_win.pkl"
WEIGHTS_PKL_FILE_LIN = "data/model/vgg19_normalized_lin.pkl"


####################
# NEURAL NETWORK
####################
class CustomConv(Convolution):

    def __init__(self, layer, num_filters, filter_size):
        Convolution.__init__(self, layer, num_filters, filter_size,
                             pad=1, flip_filters=False)


class CustomPool(Pooling):

    def __init__(self, layer):
        Pooling.__init__(self, layer, pool_size=2, mode="average_exc_pad")


def build_model():
    '''
    Use VGG-19, 19-layer model from the paper:
    "Very Deep Convolutional Networks for Large-Scale Image Recognition",
    Karen Simonyan & Andrew Zisserman
    '''
    vgg19 = {}
    vgg19["input"] = InputLayer((1, 3, IMAGE_SIZE, IMAGE_SIZE))
    vgg19["conv1_1"] = CustomConv(vgg19["input"], 64, 3)
    vgg19["conv1_2"] = CustomConv(vgg19["conv1_1"], 64, 3)
    vgg19["pool1"] = CustomPool(vgg19["conv1_2"])

    vgg19["conv2_1"] = CustomConv(vgg19["pool1"], 128, 3)
    vgg19["conv2_2"] = CustomConv(vgg19["conv2_1"], 128, 3)
    vgg19["pool2"] = CustomPool(vgg19["conv2_2"])

    vgg19["conv3_1"] = CustomConv(vgg19["pool2"], 256, 3)
    vgg19["conv3_2"] = CustomConv(vgg19["conv3_1"], 256, 3)
    vgg19["conv3_3"] = CustomConv(vgg19["conv3_2"], 256, 3)
    vgg19["conv3_4"] = CustomConv(vgg19["conv3_3"], 256, 3)
    vgg19["pool3"] = CustomPool(vgg19["conv3_4"])

    vgg19["conv4_1"] = CustomConv(vgg19["pool3"], 512, 3)
    vgg19["conv4_2"] = CustomConv(vgg19["conv4_1"], 512, 3)
    vgg19["conv4_3"] = CustomConv(vgg19["conv4_2"], 512, 3)
    vgg19["conv4_4"] = CustomConv(vgg19["conv4_3"], 512, 3)
    vgg19["pool4"] = CustomPool(vgg19["conv4_4"])

    vgg19["conv5_1"] = CustomConv(vgg19["pool4"], 512, 3)
    vgg19["conv5_2"] = CustomConv(vgg19["conv5_1"], 512, 3)
    vgg19["conv5_3"] = CustomConv(vgg19["conv5_2"], 512, 3)
    vgg19["conv5_4"] = CustomConv(vgg19["conv5_3"], 512, 3)
    vgg19["pool5"] = CustomPool(vgg19["conv5_4"])

    return vgg19


def load_pretrained_weights(net):
    """
    Initializes the network weights with VGG19 pre-trained weights (inplace)
    IN: net - neural network model
    """
    if ("Windows" in platform.system()):
        weights_file = WEIGHTS_PKL_FILE_WIN
    else:
        weights_file = WEIGHTS_PKL_FILE_LIN
    print(weights_file)
    assert(os.path.isfile(weights_file))
    values = pickle.load(open(weights_file))["param values"]
    lasagne.layers.set_all_param_values(net["pool5"], values)


def get_vgg_model():
    '''
    Returns the "trained" model (with both proper architecture and weights).
    '''
    net = build_model()
    load_pretrained_weights(net)
    return net
####################
# END NEURAL NETWORK
####################


####################
# IMAGES
####################
def prep_image(im_path):
    '''
    Prepares the image for input (resize to a fixed size; align RGB axis to the
    one expected by the VGG model; normalize with precomputed ImageNet mean)
    '''
    im = plt.imread(im_path)
    # resize input image into a square image
    if len(im.shape) == 2:
        im = im[:, :, np.newaxis]
        im = np.repeat(im, 3, axis=2)
    h, w, _ = im.shape
    if h < w:
        im = skimage.transform.resize(
            im, (IMAGE_SIZE, w * IMAGE_SIZE / h), preserve_range=True)
    else:
        im = skimage.transform.resize(
            im, (h * IMAGE_SIZE / w, IMAGE_SIZE), preserve_range=True)

    # Central crop
    h, w, _ = im.shape
    im = im[h // 2 - IMAGE_SIZE // 2:h // 2 + IMAGE_SIZE //
            2, w // 2 - IMAGE_SIZE // 2:w // 2 + IMAGE_SIZE // 2]

    rawim = np.copy(im).astype("uint8")

    # Shuffle axes to c01
    im = np.swapaxes(np.swapaxes(im, 1, 2), 0, 1)

    # Convert RGB to BGR
    im = im[::-1, :, :]

    im = im - MEAN_VALUES
    return rawim, floatX(im[np.newaxis])


def deprocess(im):
    '''
    Reverts some prep_image changes from x, in order to properly plot it.
    '''
    im = np.copy(im[0])
    im += MEAN_VALUES

    im = im[::-1]
    im = np.swapaxes(np.swapaxes(im, 0, 1), 1, 2)

    im = np.clip(im, 0, 255).astype("uint8")
    return im


def resize_figure(no_imgs):
    '''
    Pretty plotting (not important).
    '''
    dim1, dim2 = plt.rcParams['figure.figsize']
    amplitude = np.sqrt(no_imgs / 2) / 3
    if (amplitude > 0):
        dim1 *= amplitude
        dim2 *= amplitude
    if dim1 > 14:
        dim1 = 14
    if dim1 < 8:
        dim1 = 8
    if dim2 > 8:
        dim2 = 8
    if dim2 < 6:
        dim2 = 6
    plt.rcParams['figure.figsize'] = dim1, dim2


def plot_image(image, name=None):
    '''
    Pretty plotting (not important).
    '''
    if name:
        plt.savefig(OUT_FOLDER_NAME + name)

    f = plt.figure()
    f.patch.set_facecolor('white')

    ax = f.gca()
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    ax.imshow(deprocess(image))
    plt.show()
    plt.pause(SHOW_TIME)


def plot_images(images, is_processed=True):
    '''
    Pretty plotting (not important).
    '''
    no_imgs = len(images)
    rows = int(np.sqrt(no_imgs / 1.2))
    columns = (no_imgs + rows - 1) / rows
    resize_figure(no_imgs)

    fig = plt.figure()
    fig.patch.set_facecolor('white')
    fig.canvas.set_window_title("Neural Bitdefender Art")

    font = FontProperties()
    font.set_size('medium')
    font.set_family('sans-serif')

    for i, (image, title) in enumerate(images):
        ax = plt.subplot(rows, columns, i + 1)
        ax.set_axis_bgcolor('grey')
        if is_processed:
            ax.imshow(deprocess(image))
        else:
            ax.imshow(image)
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
        ax.set_title(title, fontproperties=font)
    plt.show()
    plt.pause(SHOW_TIME)


def init_plotting():
    '''
    Pretty plotting (not important).
    '''
    plt.ion()
    plt.rcParams['toolbar'] = 'None'


def generate_noise(img_size):
    '''
    Generates mean centered noisy image.
    '''
    noise = np.random.uniform(-128, 128, (1, 3, img_size, img_size))
    return theano.shared(floatX(noise))


def get_photos(art_path, photo_path):
    '''
    Loads and pre-processes 2 images.
    '''
    _, photo = prep_image(photo_path)
    _, art = prep_image(art_path)
    return art, photo
####################
# END IMAGES
####################


####################
# OTHERS
####################
def gram_matrix(x, ndim=3):
    '''
    Gram matrix is the scalar product.
    '''
    x = x.flatten(ndim=ndim)
    g = T.tensordot(x, x, axes=([ndim - 1], [ndim - 1]))
    return g


def tensor_to_4D(a_tensor, img_size):
    '''
    Transforms from flatten tensor to 4D tensor (needed for plotting).
    '''
    value = a_tensor.get_value().astype("float64")
    return value.reshape((1, 3, img_size, img_size))


####################
# EXERCISES
####################
def theano_function_example():
    '''
    In this example, you can observe how to:
    - define a symbolic variable (theano tensor)
    - define a theano function
    - evaluate the function in a specific value of the variable
    '''
    x = theano.tensor.dscalar()
    f = theano.function([x], 2 * x)
    print f(4)


def gram_matrix_example():
    '''
    Gram matrix is a fancy name for a scalar product generalization. You can
    observe how the gram matrix result is in fact the scalar product between
    input component by axis (for instance the lines).

    Gram Matrix | Input Matrix
    1, 1        | <line_1, line_1>
    1, 2 = 2, 1 | <line_1, line_2> =  <line_2, line_1>
    2, 2        | <line_2, line_2>
    '''
    print("Gram matrix example")
    noise_np = np.random.randint(0, 10, (2, 2))
    print "Sample input"
    print noise_np

    # in theano, shared() can be used to create a variable from py objects
    # http://deeplearning.net/software/theano/library/tensor/basic.html
    noise_theano = theano.shared(floatX(noise_np))
    print "Gram matrix"
    print gram_matrix(noise_theano, ndim=2).eval()
####################
# END EXERCISES
####################


def main():
    gram_matrix_example()
    theano_function_example()


if __name__ == "__main__":
    main()
